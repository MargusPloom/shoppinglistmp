package ee.bcs.valiit.shopping.controller;

import ee.bcs.valiit.shopping.data.ShoppingList;
import ee.bcs.valiit.shopping.service.ShoppingListService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/shopping")
@Slf4j
public class ShoppingListController {

    @Autowired
    ShoppingListService shoppingListService;

    @GetMapping(value = "/list", produces = "application/json")
    public List<ShoppingList> list() {
        log.debug("Shoppinglist called ...");
        List<ShoppingList> list = shoppingListService.list();
        list.sort((o1, o2) -> {
            if (o1.getId().compareTo(o2.getId()) > 0) {
                return 1;
            } else if (o1.getId().compareTo(o2.getId()) < 0) {
                return -1;
            }
            return 0;
        });
        return list;
    }

    @GetMapping(value = "/get/{id}", produces = "application/json")
    public ShoppingList get(@PathVariable Long id) {
        return shoppingListService.get(id);
    }

    @GetMapping(value = "/Tabel/{id}", produces = "application/json") // TEHTUD KONTROLLIMAKS KUIDAS TÖÖTAB MAPPING (Alert "Sain" SAVE nupult)
    public String Tabel (@PathVariable Long id) {
        return "Sain";
    }

    @PostMapping(value = "/delete/{id}", produces = "application/json")
    public void delete(@PathVariable  Long id) {
        ShoppingList shoppingList = shoppingListService.get(id);
        shoppingListService.delete(shoppingList);
    }

    @PostMapping(value = "/save")
    public void save(@RequestBody ShoppingList shoppingList) {
        ShoppingList dbShoppingList = shoppingListService.get(shoppingList.getId());
        dbShoppingList.setShoppingListName(shoppingList.getShoppingListName());
        dbShoppingList.setShoppingListDescription(shoppingList.getShoppingListDescription());
        shoppingListService.save(dbShoppingList);
    }

    @PostMapping(value = "/add")
    public void add(@RequestBody ShoppingList shoppingList) {
        shoppingListService.save(shoppingList);
    }


}
