DROP TABLE IF EXISTS shopping_list CASCADE;

CREATE TABLE shopping_list (
    id BIGSERIAL PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    description VARCHAR(200)
);

DROP TABLE IF EXISTS category CASCADE;

CREATE TABLE category (
    id BIGSERIAL PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    description VARCHAR(200)

);

DROP TABLE IF EXISTS item CASCADE;

CREATE TABLE item (
    id BIGSERIAL PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    shopping_list_id BIGINT REFERENCES shopping_list(id) NULL,
    category_id BIGINT REFERENCES category(id) NOT NULL,
    checked BOOLEAN NOT NULL DEFAULT FALSE,
    description VARCHAR(200)

);
