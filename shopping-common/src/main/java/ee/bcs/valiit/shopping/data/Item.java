package ee.bcs.valiit.shopping.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "item")
@Data
@EqualsAndHashCode(callSuper = false)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Item extends BaseEntity{

    @Column(name = "name")
    private String itemName;

    @Column(name = "category_id")
    private Long categoryId;

    @Column(name = "description")
    private String itemDescription;

    @Column(name = "shopping_list_id")
    private Long shoppingListId;

    @Column(name = "checked")
    private Boolean checked;

}
