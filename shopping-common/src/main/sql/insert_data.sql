INSERT INTO CATEGORY (name, description) VALUES ('Toidukaubad', 'Kõik, mis hamba all ei karju');
INSERT INTO CATEGORY (name, description) VALUES ('Tööstuskaubad', 'Elutud asjad kodus');
INSERT INTO CATEGORY (name, description) VALUES ('Lemmikloomakaubad', 'Loomale vajalikud asjad');

INSERT INTO ITEM (name, category_id, description) VALUES ('Tere piim', 1, 'kontrolli kuupäeva!');
INSERT INTO ITEM (name, category_id, description) VALUES ('Leiburi rukkipala', 1, 'kontrolli kuupäeva-värskemad allpool kastis!');
INSERT INTO ITEM (name, category_id, description) VALUES ('Sidrun', 1, 'Vali hea sidrun!');

INSERT INTO ITEM (name, category_id, description) VALUES ('Seinavärv, roosa', 2, 'Võta suurem purk');
INSERT INTO ITEM (name, category_id, description) VALUES ('Põrandavärv, roosa', 2, 'Võta suur purk');
INSERT INTO ITEM (name, category_id, description) VALUES ('Laevärv', 2, 'Võta kohe rohkem');

INSERT INTO ITEM (name, category_id, description) VALUES ('Royal Canin krõbinad', 3, 'Võta nii suur kott kui on');
INSERT INTO ITEM (name, category_id, description) VALUES ('Shampoon', 3, 'Tundlikule nahale');
INSERT INTO ITEM (name, category_id, description) VALUES ('Närimiskont', 3, 'paar keskmist');

