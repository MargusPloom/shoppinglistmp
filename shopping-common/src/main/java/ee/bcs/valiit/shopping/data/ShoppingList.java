package ee.bcs.valiit.shopping.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "shopping_list")
@Data
@EqualsAndHashCode(callSuper = false)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ShoppingList extends BaseEntity {

    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String shoppingListName;

    @Column(name = "description")
    private String shoppingListDescription;

    @OneToMany(fetch = FetchType.EAGER)
    @Fetch(FetchMode.SELECT)
    @JoinColumn(name = "shopping_list_id")
    @OrderBy("name asc")
    private List<Item> items;

}
