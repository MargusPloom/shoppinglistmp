package ee.bcs.valiit.shopping.service;


import ee.bcs.valiit.shopping.data.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class ShoppingListService {

    @Autowired
    private ShoppingListRepository shoppingListRepository;
    private ItemRepository itemRepository;
    private CategoryRepository categoryRepository;

    public List<ShoppingList> list() {
        return shoppingListRepository.findAll();
    }

    public ShoppingList get(Long id) {
        return shoppingListRepository.getOne(id);
    }

    @Transactional
    public void delete(ShoppingList shoppingList) {
        shoppingListRepository.delete(shoppingList);
    }

    @Transactional
    public void save(ShoppingList shoppingList) {
        ShoppingList dbShoppingList = shoppingListRepository.save(shoppingList);
        if (shoppingList.getId() == 0) {
            ShoppingList shoppingList1 = new ShoppingList();
                }
            }
        }
